﻿import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule } from '@angular/forms';
import { FileUploadModule } from 'ng2-file-upload';

import { AppComponent } from './app.component';
import { PotetComponent } from './potet.component';
import { SimpleDemoComponent } from './simple-file-upload-demo';

@NgModule({
    imports: [
        BrowserModule,
        FormsModule,
        FileUploadModule,
    ],
    declarations: [
        AppComponent,
        PotetComponent,
        SimpleDemoComponent,
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }
