﻿import { Component, Input, OnInit } from '@angular/core';

import { Potet } from './potet';

@Component({
    selector: 'Potet',
    templateUrl: '../../Content/Potet/potet.component.html',
    // styleUrls: [ './organizer-edit.component.css' ]
})
export class PotetComponent implements OnInit {
    potet: Potet;

    ngOnInit(): void {
        this.potet = { name: "Great Name" };
    }

}


