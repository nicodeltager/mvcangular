﻿import { Component } from '@angular/core';

import { PotetComponent } from './potet.component';

@Component({
    selector: 'helloworld',
    template: `
        <h1>Page Says: hello magnus</h1>
        <Potet>
        </Potet>
    `
})
export class AppComponent {
}
